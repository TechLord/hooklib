Overview:
Very simple hooking library without disassembler. See example.cpp for
an example.

This hooking library is NOT thread-safe, it's intended for very small
project that don't require thread-safety.

License:
Feel free to use it wherever you like, just don't sell it. Credits are
appreciated, but not required.